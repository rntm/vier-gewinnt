/**
 * @author  user
 */
public class Board {
	/**
	 * @author  user
	 */
	enum player {
		red, green, empty;
	}

	private int[][] board = new int[6][7];

	public Board(int[][] board)
	{
		this.board = board;
	}
	public Board() {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				this.board[i][j] = player.empty.ordinal();
			}
		}

	}

	public void setMove(int y, int x, player player) {
		this.board[y][x] = player.ordinal();
	}

	// public boolean isOver()
	// {
	//
	// }
	public int getLengthY() {
		return this.board.length;
	}

	public int getLengthX() {
		return this.board[0].length;
	}

	public int[][] getBoard() {
		return this.board;
	}

	public void printBoard() {
		for (int i = this.board.length - 1; i >= 0; i--) {
			System.out.print(" | ");
			for (int j = this.board[0].length - 1; j >= 0; j--) {
				if (this.board[i][j] == player.red.ordinal()) {
					System.out.print("x");
				} else if (this.board[i][j] == player.green.ordinal()) {
					System.out.print("o");
				} else {
					System.out.print(" ");
				}
				System.out.print(" | ");
			}
			System.out.println();
		}
	}
}
