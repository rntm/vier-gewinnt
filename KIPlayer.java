
/**
 * @author  user
 */
public class KIPlayer {
	private int[][] board;
	private int maxValue = 0;
	private int tmpX;
	private int x;
	private Board.player bot = Board.player.red;
	private Board.player enemy = Board.player.green;
	private Board boardObject = null;
	private MatchEngine engine = null;

	public void setBoard(int[][] board) {
		this.board = board;
		this.boardObject = new Board(this.board);
		this.engine = new MatchEngine(this.boardObject);
		this.maxValue = 0;
	}
	
	public int getX()
	{
		return this.x;
	}

	public int getBestMove(Board.player player) {
		for (int i = 0; i < 7; i++) {
			if (setMove(i, player)) {
				if (engine.isOver(player)) {
					removeLastMove(i);
					return i;
				}
				removeLastMove(i);
			}
		}
		return -1;
	}

	public void backTrackMove(Board.player player, int step) {
		if (step == 0) { // pr�fen ob direkt mit dem ersten Zug gewonnen werden kann
			int bestMove = getBestMove(this.bot);
			if (bestMove != -1) {
				this.x = bestMove;
				return; // KI gewinnt
			}
			bestMove = getBestMove(this.enemy);
			if (bestMove != -1) {
				this.x = bestMove;
				return; // Gegner gewinnt
			}

		}
		if (step < 6) {
			// backtracking f�r 6 Z�ge
			for (int i = 0; i < 7; i++) {
				if (step == 0) {
					this.tmpX = i;
				}
				if (setMove(i, player)) {
					if (!engine.isOver(player)) {
						backTrackMove(nextPlayer(player), step + 1);
					}
					removeLastMove(i);
				}
			}
		} else {
			int value = checkBoardValue(this.bot);// gibt Anzahl offener 3er zur�ck
			if (value > this.maxValue) {
				printBoard();
				this.maxValue = value; // merkt sich Anzahl offener 3er
				this.x = this.tmpX; // merkt sich Position f�r den Stein der gesetzt werden muss
			}
		}
	}

	public Board.player nextPlayer(Board.player player) {
		if (player == bot) {
			return enemy;
		} else {
			return bot;
		}
	}

	public void removeLastMove(int x) {
		for (int i = this.board.length - 1; i >= 0; i--) {
			if (this.board[i][x] != Board.player.empty.ordinal()) {
				setMove(i, x, Board.player.empty);
				return;
			}
		}
	}

	public boolean setMove(int position, Board.player player) {
		for (int i = 0; i < this.board.length; i++) {
			if (this.board[i][position] == Board.player.empty.ordinal()) {
				setMove(i, position, player);
				return true;
			}
		}
		return false;
	}

	public void setMove(int x, int y, Board.player player) {
		if (player == bot) {
			this.board[x][y] = bot.ordinal();
		} else if (player == enemy) {
			this.board[x][y] = enemy.ordinal();

		} else {
			this.board[x][y] = Board.player.empty.ordinal();
		}
	}

	public int checkBoardValue(Board.player player) {
		int count = 0;
		for (int i = 0; i < 7; i++) {
			if (setMove(i, player)) {
				if (engine.isOver(player)) {
					count++;
					// if(count > 1)
					// {
					// printBoard();
					// }

				}
				removeLastMove(i);
			}
		}
		return count;
	}

	public void printBoard() {
		for (int i = this.board.length - 1; i >= 0; i--) {
			System.out.print(" | ");
			for (int j = this.board[0].length - 1; j >= 0; j--) {
				if (this.board[i][j] == Board.player.red.ordinal()) {
					System.out.print("x");
				} else if (this.board[i][j] == Board.player.green.ordinal()) {
					System.out.print("o");
				} else {
					System.out.print(" ");
				}
				System.out.print(" | ");
			}
			System.out.println();
		}
		System.out.println("--------------------------");
	}

}
