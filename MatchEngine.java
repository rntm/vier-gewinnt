import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author  user
 */
public class MatchEngine {
	private Board board = null;
	private boolean isOver = false;

	public MatchEngine(Board board)
	{
		this.board = board;
	}
	public MatchEngine() {
		this.board = new Board();
	}

	public void run() {
		KIPlayer bot = new KIPlayer();
		Board.player currentPlayer = Board.player.red;
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		while (!this.isOver) {
			System.out.println("Spieler " + currentPlayer
					+ " bitte Zug eingeben");
			String eingabe = null;
			try {
				int value = 0;
				if(currentPlayer == Board.player.red)
				{
					bot.setBoard(this.board.getBoard());
					bot.backTrackMove(currentPlayer, 0);
					value = bot.getX();
				}
				else
				{
					eingabe = br.readLine();
					value = Integer.parseInt(eingabe);
				}

				
				if (value >= 0 && value <= 6) {
					if (this.setMove(value, currentPlayer)) {
						this.board.printBoard();
						this.isOver = this.isOver(currentPlayer);
						currentPlayer = this.nextPlayer(currentPlayer);
					} else {

						System.out
								.println("Falsche Eingabe, bitte Zug erneut eingeben");
					}
				} else {
					System.out
							.println("Falsche Eingabe, bitte Zug erneut eingeben");
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out
						.println("Falsche Eingabe, bitte Zug erneut eingeben");
			}

		}
		System.out.println("Spieler " + this.nextPlayer(currentPlayer)
				+ " gewinnt");
	}

	public Board.player nextPlayer(Board.player player) {
		if (player == Board.player.red) {
			return Board.player.green;
		} else {
			return Board.player.red;
		}
	}

	public boolean setMove(int x, Board.player player) {
		for (int i = 0; i < this.board.getLengthY(); i++) {
			if (this.board.getBoard()[i][x] == Board.player.empty.ordinal()) {
				this.board.setMove(i, x, player);
				return true;
			}
		}
		return false;
	}

	public boolean isOver(Board.player player) {
		return senkrecht(4, player) || waagerecht(4, player)
				|| diagonalLeft(4, player) || diagonalRight(4, player);
	}

	private boolean senkrecht(int value, Board.player player) {
		int count = 0;
		int tmp = this.board.getBoard()[0][0];
		for (int k = 0; k < this.board.getLengthX(); k++) {
			count = 0;
			for (int i = 0; i < this.board.getLengthY(); i++) {
				tmp = this.board.getBoard()[i][k];
				if (tmp == (player.ordinal())) {
					if (this.board.getBoard()[i][k] == tmp) {
						count++;
					} else {
						count = 1;
					}
				} else {
					count = 0;
				}
				tmp = this.board.getBoard()[i][k];
				if (count == value) {
//					System.out.println("senkrecht");
					return true;
				}

			}
		}

		return false;
	}

	public boolean waagerecht(int value, Board.player player) {
		int count = 0;
		int tmp = this.board.getBoard()[0][0];
		for (int k = 0; k < this.board.getLengthY(); k++) {
			count = 0;
			for (int i = 0; i < this.board.getLengthX(); i++) {
				tmp = this.board.getBoard()[k][i];
				if (tmp == (player.ordinal())) {
					if (this.board.getBoard()[k][i] == tmp) {
						count++;
					} else {
						count = 1;
					}
				} else {
					count = 0;
				}
				tmp = this.board.getBoard()[k][i];
				if (count == value) {
//					System.out.println("waagerecht");
					return true;
				}

			}
		}

		return false;
	}

	public boolean diagonalLeft(int value, Board.player player) {
		int count = 0;
		int count1 = 0;
		int tmp = this.board.getBoard()[0][0];
		for (int k = 0; k < this.board.getLengthY(); k++) {
			for (int i = 0; i < this.board.getLengthX(); i++) {
				count = i;
				count1 = 0;
				tmp = this.board.getBoard()[0][i];

				for (int j = k; j < this.board.getLengthY(); j++) {
					if (this.board.getLengthY() > count && count <= 5) {

						if (this.board.getBoard()[j][count] == (player
								.ordinal())) {
							if (this.board.getBoard()[j][count] == tmp) {
								count1++;
							} else {
								count1 = 1;
							}
						} else {
							count1 = 0;
						}

						tmp = this.board.getBoard()[j][count];
						if (count1 == value) {
//							System.out.println("dl");
							return true;
						}
						// System.out.println(j + " " + count);
						count++;
					}
				}

			}
		}

		return false;
	}

	public boolean diagonalRight(int value, Board.player player) {
		int count = 0;
		int count1 = 0;
		int tmp = this.board.getBoard()[0][0];
		for (int k = 0; k < this.board.getLengthY(); k++) {
			for (int i = this.board.getLengthX() - 1; i >= 0; i--) {
				count = i;
				count1 = 0;
				tmp = this.board.getBoard()[0][i];
				for (int j = k; j < this.board.getLengthY(); j++) {
					if (this.board.getLengthX() > count && count >= 0) {

						if (this.board.getBoard()[j][count] == (player
								.ordinal())) {

							if (this.board.getBoard()[j][count] == tmp) {
								count1++;
							} else {
								count1 = 1;
							}
						} else {
							count1 = 0;
						}

						tmp = this.board.getBoard()[j][count];
						if (count1 == value) {
//							System.out.println("dr");
							return true;
						}
						// System.out.println(j + " " + count);
						count--;
					}
				}

			}
		}

		return false;
	}

	public void print() {
		this.board.printBoard();
	}

}
